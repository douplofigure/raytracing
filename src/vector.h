#ifndef VECTOR_H
#define VECTOR_H

#include <cstdarg>
#include <cmath>
#include <iostream>


template<unsigned int dim, typename T = double> class Vector {

    public:
        /**This will copy data, you can free it after the call*/
        Vector(T * data){
            for (int i = 0; i < dim; ++i) {
                this->data[i] = data ? data[i] : (T) 0;
            }
        }

        Vector() {
            for (int i = 0; i < dim; ++i)
                this->data[i] = (T) 0;
        }

        Vector(T t0, ...) {
            this->data[0] = t0;
            va_list vl;
            va_start(vl, NULL);
            for (int i = 1; i < dim; ++i)
                this->data[i] = va_arg(vl, T);
            va_end(vl);

        }

        virtual ~Vector() {

        }

        T& operator[](int i) {
            if (i < 0 || i >= dim)
                throw std::runtime_error("No such coordinate");
            return data[i];
        }

        T operator*(Vector<dim, T> vec) {
            T val = (T) 0;
            for (int i = 0; i < dim; ++i) {
                val = val + this->data[i] * vec.data[i];
            }
            return val;
        }

        Vector<dim, T> operator+(Vector<dim, T> vec) {

            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] + vec.data[i];
            }
            return Vector<dim, T>(tmp);

        }

        Vector<dim, T> operator*(T v) {

            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] *v;
            }
            return Vector<dim, T>(tmp);

        }

        Vector<dim, T> operator-(Vector<dim, T> vec) {

            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] - vec.data[i];
            }
            return Vector<dim, T>(tmp);

        }

        inline Vector<dim, T> operator-() {

            return (T) -1 * (*this);

        }

        Vector<dim, T> operator/(T v) {

            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] / v;
            }
            return Vector<dim, T>(tmp);

        }

        void normalize() {
            T val = this->length();
            for (int i = 0; i < dim; ++i) {
                this->data[i] /= val;
            }
        }

        Vector<dim, T> normalized() {
            T val = this->length();
            T tmp[dim];
            for (int i = 0; i < dim; ++i) {
                tmp[i] = data[i] / val;
            }
            return Vector<dim, T>(tmp);
        }

        T length() {
            return (T) sqrt(length2());
        }

        T length2() {
            T sum = (T) 0;
            for (int i = 0; i < dim; ++i) {
                sum += this->data[i] * this->data[i];
            }
            return sum;
        }

    protected:

    private:

        T data[dim];

};

#ifdef __SSE_MATH__
#include <emmintrin.h>
#endif // __SSE_MATH__

template <> class Vector<3, double> {


    public:
        Vector(double x, double y, double z) {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        Vector(double d[3]) {
            this->x = d[0];
            this->y = d[1];
            this->z = d[2];
        }

        Vector() {
            this->x = this->y = this->z = 0;
        }

        double& operator[] (int i) {
            //if (i < 0 || i > 2) throw std::runtime_error("Not in vector");
            switch (i) {
                case 0:
                    return x;
                case 1:
                    return y;
                case 2:
                    return z;
                default:
                    throw std::runtime_error("Not in vector");
            }
        }

        Vector<3, double> operator+(Vector<3, double> v) {


            /*#ifdef __SSE_MATH__
            Vector<3, double> tmp;

            __m128d tv = _mm_loadu_pd(&x);
            __m128d vv = _mm_loadu_pd(&v.x);
            __m128d r = _mm_add_pd(tv, vv);

            _mm_storeu_pd(&tmp.x, r);

            tmp.z = z + v.z;
            return tmp;

            #endif // __SSE_MATH__*/
            return Vector<3, double>(x+v.x, y+v.y, z+v.z);
        }

        Vector<3, double> operator-(Vector<3, double> v) {
            return Vector<3, double>(x-v.x, y-v.y, z-v.z);
        }

        Vector<3, double> operator-() {
            return Vector<3, double>(-x, -y, -z);
        }

        Vector<3, double> operator*(double d) {
            return Vector<3, double>(x*d, y*d, z*d);
        }

        double operator* (Vector<3, double> v) {
            return x * v.x + y * v.y + z*v.z;
        }


        Vector<3, double> operator/(double d) {
            return Vector<3, double>(x/d, y/d, z/d);
        }

        void normalize() {
            double val = this->length();
            x /= val;
            y /= val;
            z /= val;
        }

        Vector<3, double> normalized() {
            double val = this->length();
            return Vector<3, double>(x/val, y/val, z/val);
        }

        double length() {
            return sqrt(length2());
        }

        double length2() {
            return x * x + y * y + z * z;
        }

    private:
        double x;
        double y;
        double z;

};

template <> class Vector<3, float> {


    public:
        Vector(float x, float y, float z) {
            this->x = x;
            this->y = y;
            this->z = z;
        }

        Vector(float d[3]) {
            this->x = d[0];
            this->y = d[1];
            this->z = d[2];
        }

        Vector() {
            this->x = this->y = this->z = 0;
        }

        float& operator[] (int i) {
            //if (i < 0 || i > 2) throw std::runtime_error("Not in vector");
            switch (i) {
                case 0:
                    return x;
                case 1:
                    return y;
                case 2:
                    return z;
                default:
                    throw std::runtime_error("Not in vector");
            }
        }

        Vector<3, float> operator+(Vector<3, float> v) {


            #ifdef __SSE_MATH__
            Vector<3, float> tmp;

            __m128 tv = _mm_loadu_ps(&x);
            __m128 vv = _mm_loadu_ps(&v.x);
            __m128 r = _mm_add_ps(tv, vv);

            _mm_storeu_ps(&tmp.x, r);

            //tmp.z = z + v.z;
            return tmp;

            #endif // __SSE_MATH__

            /*Vector<3, double> tmp;

            asm ("movupd (%1), %%xmm0;\n"
                 "movupd (%2), %%xmm1;\n"
                 "addpd %%xmm0, %%xmm1;\n"
                 "movupd %%xmm1, (%0);\n"
                 : "=r"(tmp.x)
                 : "r" (&x), "r"(&v.x)
                 : "%xmm0", "%xmm1"
            );

            tmp.z = z + v.z;

            return tmp;*/
            return Vector<3, float>(x+v.x, y+v.y, z+v.z);
        }

        Vector<3, float> operator-(Vector<3, float> v) {
            return Vector<3, float>(x-v.x, y-v.y, z-v.z);
        }

        Vector<3, float> operator-() {
            return Vector<3, float>(-x, -y, -z);
        }

        Vector<3, float> operator*(float d) {
            return Vector<3, float>(x*d, y*d, z*d);
        }

        float operator* (Vector<3, float> v) {
            return x * v.x + y * v.y + z*v.z;
        }


        Vector<3, float> operator/(float d) {
            return Vector<3, float>(x/d, y/d, z/d);
        }

        void normalize() {
            float val = this->length();
            x /= val;
            y /= val;
            z /= val;
        }

        Vector<3, float> normalized() {
            float val = this->length();
            return Vector<3, float>(x/val, y/val, z/val);
        }

        float length() {
            return sqrt(length2());
        }

        float length2() {
            return x * x + y * y + z * z;
        }

    private:
        float x;
        float y;
        float z;
        float t;

};

template <unsigned int dim, typename T> inline Vector<dim, T> operator*(T a, Vector<dim, T> vec) {
    T tmp[dim];
    for (int i = 0; i < dim; ++i) {
        tmp[i] = vec[i] * a;
    }
    return Vector<dim, T>(tmp);
}

template <unsigned int dim, typename T> inline std::ostream& operator<<(std::ostream& stream, Vector<dim, T> vec) {
    stream << "(";
    stream << vec[0];
    for (int i = 1; i < dim; ++i) {
        stream << ", " << vec[i];
    }
    stream << ")";
    return stream;
}

#endif // VECTOR_H

