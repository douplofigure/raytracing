#ifndef CAMERA_H
#define CAMERA_H

#include "vector.h"
#include "quaternion.h"
#include "ray.h"

class Camera
{
    public:
        Camera();
        Camera(double vfov, double aspect, Vector<3> pos, Quaternion rot);
        virtual ~Camera();

        Ray getRay(double u, double v);

    protected:

    private:

        Vector<3> position;
        Vector<3> llcorner;
        Vector<3> horizontal;
        Vector<3> vertical;

        Quaternion rotation;

};

#endif // CAMERA_H
