#include "color.h"

#include "../matrix.h"

Color::Color(std::shared_ptr<Spectrum> s) {
    this->spec = s;
}

Color::~Color() {
    //dtor
}

Vector<3> Color::rgb() {
    return cie2rgb(spec->getCIEColor());
}

Vector<3> Color::cie() {
    return spec->getCIEColor();
}

Vector<3> Color::cie2rgb(Vector<3> cieColor) {

    double mData[9] = {
        3.24096994, -1.53738318, -0.49861076,
        -0.96924364, 1.8759675, 0.04155506,
        0.05563008, -0.20397696, 1.05697151
    };

    Matrix<3,3> m(mData);

    Vector<3> linear = m * cieColor;

    return linear;

}

Vector<3> Color::rgb2cie(Vector<3> rgbColor) {

    double mData[9] = {
        0.41239080, 0.35758434, 0.18048079,
        0.21263901, 0.71516868, 0.07219232,
        0.01933082, 0.11919478, 0.95053215
    };

    Matrix<3,3> m(mData);

    return m * rgbColor;

}
