#ifndef SPECTRUM_H
#define SPECTRUM_H

#include <memory>
#include <vector>
#include <ostream>

#include "../vector.h"

class Spectrum {
    public:
        Spectrum();
        virtual ~Spectrum();

        virtual double getIntensity(double lambda);

        virtual Vector<3> getCIEColor();
        static void printPlot(std::shared_ptr<Spectrum> spec, std::ostream & stream);

        static void precomputeIntegralValues();

    protected:

        static const unsigned int integralSamples = 10;
        static std::vector<double> xValues;
        static std::vector<double> wValues;
        //static std::vector<Vector<3>> cieValues;

    private:

};

class Blackbody : public Spectrum {

    public:
        Blackbody(double T);
        virtual ~Blackbody();

        double getIntensity(double lambda);

    private:
        double temperature;

};

class DiscreteSpectrum : public Spectrum {

    public:
        DiscreteSpectrum(std::shared_ptr<Spectrum> spec);
        DiscreteSpectrum(Spectrum & spec);
        virtual ~DiscreteSpectrum();

        Vector<3> getCIEColor() override;

        static std::shared_ptr<DiscreteSpectrum> multplied(std::shared_ptr<DiscreteSpectrum> s, double d);
        static std::shared_ptr<DiscreteSpectrum> added(std::shared_ptr<DiscreteSpectrum> s1, std::shared_ptr<DiscreteSpectrum> s2);

    protected:
        DiscreteSpectrum(std::vector<double> value);

    private:

        std::vector<double> values;

};

std::shared_ptr<DiscreteSpectrum> operator*(double d, std::shared_ptr<DiscreteSpectrum> s);
std::shared_ptr<DiscreteSpectrum> operator+(std::shared_ptr<DiscreteSpectrum> s1, std::shared_ptr<DiscreteSpectrum> s2);

class AdditiveSpectrum : public Spectrum {

    public:
        AdditiveSpectrum(double w1, std::shared_ptr<Spectrum> s1, double w2, std::shared_ptr<Spectrum> s2);

        double getIntensity(double lambda);

    private:
        double w1;
        double w2;
        std::shared_ptr<Spectrum> s1;
        std::shared_ptr<Spectrum> s2;

};

#include "../spline.h"

class SplineSpectrum : public Spectrum {

    public:
        SplineSpectrum(std::vector<double> interleavedData);
        SplineSpectrum(std::vector<double> lambdas, std::vector<double> values);

        double getIntensity(double lambda);

    private:

        Math::Spline<double> spline;

};

class ConstantSpectrum : public Spectrum {

    public:
        ConstantSpectrum(double value);

        double getIntensity(double lambda);

    private:
        double v;


};

class MultSpectrum : public Spectrum {

    public:
        MultSpectrum(std::shared_ptr<Spectrum> a, std::shared_ptr<Spectrum> b);

        double getIntensity(double lambda);

    private:
        std::shared_ptr<Spectrum> a;
        std::shared_ptr<Spectrum> b;

};

class CIESpectrum : public Spectrum {

    public:
        CIESpectrum(Vector<3> cieColor);

        double getIntensity(double lambda);
        Vector<3> getCIEColor();

    private:
        Vector<3> cieColor;

};

std::shared_ptr<Spectrum> operator*(double d, std::shared_ptr<Spectrum> s);

#endif // SPECTRUM_H
