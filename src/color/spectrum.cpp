#include "spectrum.h"

#include <math.h>
#include <functional>

#include "../matrix.h"
#include "../mathutils.h"

std::vector<double> Spectrum::xValues;
std::vector<double> Spectrum::wValues;

Spectrum::Spectrum() {
    //ctor
}

Spectrum::~Spectrum() {
    //dtor
}

void Spectrum::precomputeIntegralValues() {

    xValues = std::vector<double>(integralSamples);
    wValues = std::vector<double>(integralSamples);

    gauleg(380e-9, 780e-9, xValues.data(), wValues.data(), integralSamples);

}

double Spectrum::getIntensity(double lambda) {
    return 0.0;
}

double gaussian(double x, double alpha, double mu, double sigma1, double sigma2) {
    double squareRoot = (x - mu)/(x < mu ? sigma1 : sigma2);
    return alpha * exp( -(squareRoot * squareRoot)/2 );
}

double xBar(double lambda) {

    /// Convert to Angstroem.
    double l = lambda / 1e-10;

    return gaussian(l,  1.056, 5998, 379, 310)
            + gaussian(l,  0.362, 4420, 160, 267)
            + gaussian(l, -0.065, 5011, 204, 262);

}

double yBar(double lambda) {

    double l = lambda / 1e-10;

    return gaussian(l,  0.821, 5688, 469, 405)
            + gaussian(l,  0.286, 5309, 163, 311);

}

double zBar(double lambda) {

    double l = lambda / 1e-10;

    return gaussian(l,  1.217, 4370, 118, 360)
            + gaussian(l,  0.681, 4590, 260, 138);

}

void Spectrum::printPlot(std::shared_ptr<Spectrum> spec, std::ostream & stream) {

    for (double l = 380; l < 780; l += 1.0) {

        double lambda = l * 1e-9;
        double i = spec->getIntensity(lambda);

        std::cout << l << "\t" << i << std::endl;

    }

}

Vector<3> Spectrum::getCIEColor() {

    /// vary lambda between 380 and 780 nm.

    const double l0 = 380e-9;
    const double l1 = 780e-9;
    const unsigned int sampleCount = 80;

    double h = (l1 - l0) / (sampleCount-1);

    double value = getIntensity(l0);
    Vector<3> r;/*(value * xBar(l0) / 2.0, value * yBar(l0) / 2.0, value * zBar(l0) / 2.0);

    for (unsigned int i = 1; i < sampleCount; ++i) {

        double lambda = l0 + h * i;

        value = getIntensity(lambda);

        r = r + Vector<3>(value * xBar(lambda), value * yBar(lambda), value * zBar(lambda));

    }*/

    double x=0, y=0, z=0;

    for (unsigned int i = 0; i < integralSamples; ++i) {

        double lambda = xValues[i];
        value = getIntensity(lambda);

        x += wValues[i] * (value * xBar(lambda));
        y += wValues[i] * (value * yBar(lambda));
        z += wValues[i] * (value * zBar(lambda));

    }

    value = getIntensity(l1);
    /*r = r + Vector<3>(value * xBar(l1), value * yBar(l1), value * zBar(l1)) / 2.0;
    r = h * r;
    */
    r = Vector<3>(x,y,z);

    //Vector<3> chromacity = h * r;

    //double s = chromacity[0] + chromacity[1] + chromacity[2];

    //chromacity = chromacity / s;

    return r;

}

double gamma(double u) {

    return (u <= 0.0031308 ? 12.92 * u : 1.055 * pow(u, 1/2.4) - 0.055);

}

Blackbody::Blackbody(double T) {
    this->temperature = T;
}

Blackbody::~Blackbody() {

}

double Blackbody::getIntensity(double lambda) {

    const double h = 6.62607015e-34;
    const double c = 299792458;
    const double k = 1.380649e-23;

    return ((8.0 * M_PI * h * c * c) / (lambda * lambda * lambda * lambda * lambda))  * (1.0 / (exp((h * c)/(lambda * k * temperature))- 1.0));

}

AdditiveSpectrum::AdditiveSpectrum(double w1, std::shared_ptr<Spectrum> s1, double w2, std::shared_ptr<Spectrum> s2) {

    this->w1 = w1;
    this->w2 = w2;
    this->s2 = s2;
    this->s1 = s1;

}

double AdditiveSpectrum::getIntensity(double lambda) {

    return w1 * s1->getIntensity(lambda) + w2 * s2->getIntensity(lambda);

}

DiscreteSpectrum::DiscreteSpectrum(std::shared_ptr<Spectrum> spec) {

    this->values = std::vector<double>(Spectrum::integralSamples);

    for (unsigned int i = 0; i < Spectrum::integralSamples; ++i) {
        values[i] = spec->getIntensity(Spectrum::xValues[i]);
    }

}

DiscreteSpectrum::DiscreteSpectrum(Spectrum & spec) {

    this->values = std::vector<double>(Spectrum::integralSamples);

    for (unsigned int i = 0; i < Spectrum::integralSamples; ++i) {
        values[i] = spec.getIntensity(Spectrum::xValues[i]);
    }

}

DiscreteSpectrum::DiscreteSpectrum(std::vector<double> value) {

    this->values = value;

}

DiscreteSpectrum::~DiscreteSpectrum() {

}

Vector<3> DiscreteSpectrum::getCIEColor() {

    double x=0, y=0, z=0;

    for (unsigned int i = 0; i < integralSamples; ++i) {

        double lambda = xValues[i];

        x += wValues[i] * (values[i] * xBar(lambda));
        y += wValues[i] * (values[i] * yBar(lambda));
        z += wValues[i] * (values[i] * zBar(lambda));

    }

    return Vector<3>(x,y,z);

}

std::shared_ptr<DiscreteSpectrum> DiscreteSpectrum::multplied(std::shared_ptr<DiscreteSpectrum> s, double d) {

    /// TODO use __mmx to speedup operation

    std::vector<double> vals(integralSamples);

    for (unsigned int i = 0; i < integralSamples; ++i) {
        vals[i] = d * s->values[i];
    }

    return std::shared_ptr<DiscreteSpectrum>(new DiscreteSpectrum(vals));

}

std::shared_ptr<DiscreteSpectrum> DiscreteSpectrum::added(std::shared_ptr<DiscreteSpectrum> s1, std::shared_ptr<DiscreteSpectrum> s2) {

    /// TODO: use __mmx to speed up
    std::vector<double> vals(integralSamples);

    for (unsigned int i = 0; i < integralSamples; ++i) {
        vals[i] = s1->values[i] + s2->values[i];
    }

    return std::shared_ptr<DiscreteSpectrum>(new DiscreteSpectrum(vals));

}

std::shared_ptr<DiscreteSpectrum> operator*(double d, std::shared_ptr<DiscreteSpectrum> s) {
    return DiscreteSpectrum::multplied(s, d);
}

std::shared_ptr<DiscreteSpectrum> operator+(std::shared_ptr<DiscreteSpectrum> s1, std::shared_ptr<DiscreteSpectrum> s2) {
    return DiscreteSpectrum::added(s1, s2);
}

SplineSpectrum::SplineSpectrum(std::vector<double> interleavedData) {

    std::vector<double> lambdas(interleavedData.size()/2);
    std::vector<double> values(interleavedData.size()/2);

    for (unsigned int i = 0; i < interleavedData.size()/2; ++i) {

        lambdas[i] = interleavedData[i*2];
        values[i] = interleavedData[i*2+1];

    }

    spline = Math::Spline<double>(lambdas, values);

}

SplineSpectrum::SplineSpectrum(std::vector<double> lambdas, std::vector<double> values) : spline(lambdas, values) {

}

double SplineSpectrum::getIntensity(double lambda) {

    return spline(lambda);

}

ConstantSpectrum::ConstantSpectrum(double value) {
    this->v = value;
}

double ConstantSpectrum::getIntensity(double lambda) {
    return this->v;
}

MultSpectrum::MultSpectrum(std::shared_ptr<Spectrum> sa, std::shared_ptr<Spectrum> sb) {
    this->a = sa;
    this->b = sb;

    if (!sa || ! sb) {
        throw std::runtime_error("Null-Spectrum for multiplication");
    }

}

double MultSpectrum::getIntensity(double lambda) {
    return a->getIntensity(lambda) * b->getIntensity(lambda);
}

CIESpectrum::CIESpectrum(Vector<3> cieColor) {
    this->cieColor = cieColor;
}

double CIESpectrum::getIntensity(double lambda) {
    return cieColor[0] * xBar(lambda) + cieColor[1] * yBar(lambda) + cieColor[2] * zBar(lambda);
}

Vector<3> CIESpectrum::getCIEColor() {
    return cieColor;
}


std::shared_ptr<Spectrum> operator*(double d, std::shared_ptr<Spectrum> s) {
    return std::shared_ptr<Spectrum>(new MultSpectrum(std::shared_ptr<Spectrum>(new ConstantSpectrum(d)), s));
}
