#ifndef COLOR_H
#define COLOR_H

#include "spectrum.h"

class Color {
    public:
        Color(std::shared_ptr<Spectrum> spec);
        virtual ~Color();

        virtual Vector<3> rgb();
        virtual Vector<3> cie();

        static Vector<3> cie2rgb(Vector<3> cieColor);
        static Vector<3> rgb2cie(Vector<3> rgbColor);

    protected:

    private:

        std::shared_ptr<Spectrum> spec;

};

#endif // COLOR_H
