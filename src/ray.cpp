#include "ray.h"

Ray::Ray() {

}

Ray::Ray(Vector<3> A, Vector<3> B) {
    this->A = A;
    this->B = B;
}

Ray::~Ray() {

}

Vector<3> Ray::direction() {
    return B;
}

Vector<3> Ray::origin() {
    return A;
}

Vector<3> Ray::pointAt(double t) {
    return A + t * B;
}
