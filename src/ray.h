#ifndef RAY_H
#define RAY_H

#include "vector.h"

class Ray
{
    public:
        Ray();
        Ray(Vector<3> A, Vector<3> B);
        virtual ~Ray();

        Vector<3> origin();
        Vector<3> direction();

        Vector<3> pointAt(double t);

    protected:

    private:

        Vector<3> A;
        Vector<3> B;

};

#endif // RAY_H
