#ifndef THREADQUEUE_H
#define THREADQUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

template <typename T> class ThreadQueue
{
    public:
        ThreadQueue() {

        }
        virtual ~ThreadQueue() {

        }

        T& front() {
            std::unique_lock<std::mutex> lock(mutex);
            return data.front();
        }

        T& pop() {
            std::unique_lock<std::mutex> lock(mutex);
            T& elem = data.front();
            data.pop();
            return elem;
        }

        void push_back(T&& item) {
            std::unique_lock<std::mutex> lock(mutex);
            data.push(std::move(item));
        }

        int size() {
            std::unique_lock<std::mutex> lock(mutex);
            int s = data.size();
            lock.unlock();
            return s;

        }

    protected:

    private:

        std::queue<T> data;
        std::mutex mutex;

};

#endif // THREADQUEUE_H
