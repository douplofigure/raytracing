#ifndef MATERIAL_H
#define MATERIAL_H

#include "ray.h"
#include "hitable.h"

#include "color/spectrum.h"

class Material {
    public:
        virtual bool scatter(Ray & r, HitRecord & rec, Vector<3>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world) = 0;
        virtual bool scatter(Ray & r, HitRecord & rec, std::shared_ptr<Spectrum> & reflectivity, Ray & scattered, std::shared_ptr<Hitable> world);
};


class Lambertian : public Material {

    public:

        Lambertian(Vector<3> albedo);

        bool scatter(Ray & r, HitRecord & rec, Vector<3>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world);
        bool scatter(Ray & r, HitRecord & rec, std::shared_ptr<Spectrum> & reflectivity, Ray & scattered, std::shared_ptr<Hitable> world);

    private:
        Vector<3> albedo;
        std::shared_ptr<Spectrum> refSpec;

};

class Metal : public Material {

    public:

        Metal(std::shared_ptr<Spectrum> reflectionSpectrum);
        Metal(Vector<3> albedo);

        bool scatter(Ray & r, HitRecord & rec, Vector<3>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world);
        bool scatter(Ray & r, HitRecord & rec, std::shared_ptr<Spectrum> & reflectivity, Ray & scattered, std::shared_ptr<Hitable> world);

    private:
        Vector<3> albedo;
        std::shared_ptr<Spectrum> refSpec;


};

class Glass : public Material {

    public:
        Glass(double ior);

        bool scatter(Ray & r, HitRecord & rec, Vector<3> & attenuation, Ray & scattered, std::shared_ptr<Hitable> world);
        bool scatter(Ray & r, HitRecord & rec, std::shared_ptr<Spectrum> & reflectivity, Ray & scattered, std::shared_ptr<Hitable> world);

    private:
        double ior;

};

Vector<3> randomInUnitSpere();
Vector<3> reflect(Vector<3>& , Vector<3> n);

#endif // MATERIAL_H
