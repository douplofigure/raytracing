#ifndef HITABLELIST_H
#define HITABLELIST_H

#include <vector>
#include <memory>

#include "hitable.h"


class HitableList : public Hitable
{
    public:
        HitableList();
        HitableList(std::vector<std::shared_ptr<Hitable>>& data);
        virtual ~HitableList();

        bool hit(Ray& r, double t_min, double t_max, HitRecord& rec);

        void addObject(std::shared_ptr<Hitable> obj);


    protected:

    private:

        std::vector<std::shared_ptr<Hitable>> objects;

};

#endif // HITABLELIST_H
