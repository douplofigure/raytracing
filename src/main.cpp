#include <iostream>

#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <limits>
#include <memory>
#include <thread>

#include "vector.h"
#include "ray.h"
#include "hitable.h"
#include "hitablelist.h"
#include "sphere.h"
#include "camera.h"
#include "material.h"
#include "light.h"

#include "threadqueue.h"

#include "mathutils.h"
#include "color/spectrum.h"
#include "color/color.h"

#define MAX_BOUNCES 10

#define PIXEL_AREA 1e-2
#define SHUTTER_TIME 0.001

struct RenderPiece {

    int xMin;
    int xMax;
    int yMin;
    int yMax;

};

/*Vector<3> color(Ray& ray, std::shared_ptr<Hitable> world, std::vector<std::shared_ptr<Light>>& lights, int bounce=0) {

    HitRecord rec;

    if (world->hit(ray, 0.001, std::numeric_limits<double>::max(), rec)) {

        Ray scattered;
        Vector<3> attenuation(1.0, 1.0, 1.0);
        if (bounce < MAX_BOUNCES && rec.mat->scatter(ray, rec, attenuation, scattered, world)) {
            Vector<3> col = color(scattered, world, lights, bounce + 1);

            for (int i = 0; i < lights.size(); ++i) {

                if (lights[i]->checkReached(world, rec))
                    col = col - lights[i]->getIntensityAt(rec) * (ray.direction() * rec.normal);

            }

            return Vector<3>(col[0] * attenuation[0], col[1] * attenuation[1], col[2] * attenuation[2]);
        }
        return Vector<3>(0.0, 0.0, 0.0);
    }

    Vector<3> ud = ray.direction().normalized();
    double t = 0.5 * (ud[2] + 1.0);
    return (1.0-t) * Vector<3>(1.0, 1.0, 1.0) + t * Vector<3>(0.5, 0.7, 0.8);
    //return Vector<3>(0.0, 0.0, 0.0);

}*/

std::shared_ptr<Spectrum> color(Ray & ray, std::shared_ptr<Hitable> world, std::vector<std::shared_ptr<Light>> & lights, int bounce = 0) {

    HitRecord rec;

    if (world->hit(ray, 0.001, std::numeric_limits<double>::max(), rec)) {

        Ray scattered;
        std::shared_ptr<Spectrum> attenuation;
        if (bounce < MAX_BOUNCES && rec.mat->scatter(ray, rec, attenuation, scattered, world)) {

            std::shared_ptr<Spectrum> s = color(scattered, world, lights, bounce + 1);

            for (unsigned int i = 0; i < lights.size(); ++i) {

                if (lights[i]->checkReached(world, rec)){

                    //std::cout << (lights[i]->getFalloff(rec) * Color(lights[i]->getSpectrum()).cie()) << std::endl;

                    s = std::shared_ptr<Spectrum>(new AdditiveSpectrum(1.0, s, lights[i]->getFalloff(rec) * PIXEL_AREA * SHUTTER_TIME, lights[i]->getSpectrum()));
                    //std::cout << Color(s).rgb() << std::endl;

                }

            }

            return std::shared_ptr<Spectrum>(new MultSpectrum(attenuation, s));

        }

    }

    //return 1e-8 * std::shared_ptr<Spectrum>(new Blackbody(7000));
    //return std::shared_ptr<Spectrum>(new ConstantSpectrum(1e6));
    return std::shared_ptr<Spectrum>(new Spectrum());

    //return std::shared_ptr<Spectrum>(new Blackbody(6000));

}

void outputImageData(std::vector<Vector<3>>& data, int width, int height, FILE * file = stdout) {

    fprintf(file, "P6\n%d %d\n255\n", width, height);
    uint8_t * bytes = (uint8_t *) malloc(sizeof(uint8_t) * width * height * 3);

    for (int i = 0; i < data.size(); ++i) {

        bytes[i*3] = data[i][0] * 255.99 > 255 ? 255 : data[i][0] * 255.99;
        bytes[i*3+1] = data[i][1] * 255.99 > 255 ? 255 : data[i][1] * 255.99;
        bytes[i*3+2] = data[i][2] * 255.99 > 255 ? 255 : data[i][2] * 255.99;

    }

    fwrite(bytes, sizeof(uint8_t), width * height * 3, file);

    free(bytes);

}

void renderPiece(RenderPiece& piece, int imageWidth, int imageHeight, Camera& cam, std::vector<Vector<3>>& data, int sampleCount, std::shared_ptr<Hitable> world, std::vector<std::shared_ptr<Light>>& lights) {

    for (int j = piece.yMin; j < piece.yMax; ++j) {

        for (int i = piece.xMin; i < piece.xMax; ++i) {

            Vector<3> col(0.0, 0.0, 0.0);
            std::shared_ptr<Spectrum> spec(new Spectrum());

            for (int s = 0; s < sampleCount; ++s) {

                double u = double(i + drand48()) / double(imageWidth);
                double v = double(j + drand48()) / double(imageHeight);

                Ray r = cam.getRay(u, v);
                //col = col + Color(color(r, world, lights)).rgb();
                spec = std::shared_ptr<Spectrum>(new AdditiveSpectrum(1.0, spec, 1.0, color(r, world, lights)));
            }
            col = Color(spec).rgb() / double(sampleCount);
            data[(imageHeight-j-1)*imageWidth+i] = col;// Vector<3>(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));

        }

    }

}

void threadFunc(ThreadQueue<RenderPiece>& pieces, int imageWidth, int imageHeight, Camera & camera, std::vector<Vector<3>>& data, int sampleCount, std::shared_ptr<Hitable> world, std::vector<std::shared_ptr<Light>>& lights) {

    int left = 0;

    while (left = pieces.size()) {

        try {
            RenderPiece piece = pieces.pop();
            renderPiece(piece, imageWidth, imageHeight, camera, data, sampleCount, world, lights);
        } catch (std::runtime_error e) {
            break;
        }

    }

}

#define PIECE_SIZE 128

int main() {

    const int width = 600, height = 400;

    const int sampleCount = 200;

    Vector<3> v1 = Vector<3>(-1, 2, 3);
    Vector<3> v2 = Vector<3>(2,3,-4);

    std::cout << v1 << " + " << v2 << " = " << (v1 + v2) << std::endl;

    Spectrum::precomputeIntegralValues();
    std::shared_ptr<Blackbody> bb(new Blackbody(5778));
    std::shared_ptr<DiscreteSpectrum> dbb(new DiscreteSpectrum(bb));

    //Spectrum::printPlot(bb, std::cout);

    std::cerr << (1e-6 * dbb)->getCIEColor() << std::endl;

    //exit(0);

    std::vector<double> auSplineData = {
        200e-9, 0.2,
        300e-9, 0.37,
        400e-9, 0.38,
        500e-9, 0.5,
        600e-9, 0.92,
        700e-9, 0.98,
        800e-9, 0.99
    };

    std::vector<double> agSplineData = {
        200e-9, 0.25,
        300e-9, 0.2,
        400e-9, 0.85,
        500e-9, 0.92,
        600e-9, 0.95,
        700e-9, 0.98,
        800e-9, 0.98
    };

    std::shared_ptr<Spectrum> goldSpectrum(new SplineSpectrum(auSplineData));
    std::shared_ptr<Spectrum> silverSpectrum(new SplineSpectrum(agSplineData));

    std::vector<Vector<3>> data(width * height);

    std::shared_ptr<HitableList> world(new HitableList());
    world->addObject(std::shared_ptr<Hitable>(new Sphere(Vector<3>(0.0, 4.0, 0.0), 0.5, std::shared_ptr<Material>(new Metal(goldSpectrum)))));
    world->addObject(std::shared_ptr<Hitable>(new Sphere(Vector<3>(-1.0, 2.0, 0.0), 0.5, std::shared_ptr<Material>(new Metal(goldSpectrum)))));
    //world->addObject(std::shared_ptr<Hitable>(new Sphere(Vector<3>(1.0, 2.0, 0.0), 0.5, std::shared_ptr<Material>(new Lambertian(Vector<3>(0.2, 0.2, 0.8))))));
    world->addObject(std::shared_ptr<Hitable>(new Sphere(Vector<3>(1.0, 2.0, 0.0), 0.5, std::shared_ptr<Material>(new Glass(1.458)))));
    world->addObject(std::shared_ptr<Hitable>(new Sphere(Vector<3>(1.0, 3.0, 0.0), 0.5, std::shared_ptr<Material>(new Lambertian(Color::rgb2cie(Vector<3>(0.2, 0.2, 0.8)))))));
    world->addObject(std::shared_ptr<Hitable>(new Sphere(Vector<3>(0.0, 4.0, -100.5), 100.0, std::shared_ptr<Material>(new Metal(silverSpectrum)))));

    Camera cam(70.0, (double) width / (double) height, Vector<3>(0.0, 0.0, 1.0), Quaternion::fromAxisAngle(Vector<3>(1.0, 0.0, 0.0), -M_PI / 24));

    ThreadQueue<RenderPiece> pieces;

    std::vector<std::shared_ptr<Light>> lights;

    srand48(1231243324);

    std::shared_ptr<Spectrum> sunSpectrum(new Blackbody(5778));
    std::shared_ptr<Spectrum> lampSpectrum(new Blackbody(5778));

    lights.push_back(std::shared_ptr<Light>(new PointLight(Vector<3>(0.0, 3.0, 2.0), lampSpectrum)));
    lights.push_back(std::shared_ptr<Light>(new PointLight(Vector<3>(150.0, 0.0, 130.0), sunSpectrum)));

    for (int x = 0; x < width; x += PIECE_SIZE) {

        for (int y = 0; y < height; y += PIECE_SIZE) {

            pieces.push_back((RenderPiece){x, x + PIECE_SIZE > width ? width : x+PIECE_SIZE, y, y + PIECE_SIZE > height ? height : y+PIECE_SIZE});

        }

    }

    int threadCount = std::thread::hardware_concurrency();

    std::vector<std::thread> threads(threadCount);
    for (int i = 0; i < threadCount; ++i) {

        threads[i] = std::thread([&](){threadFunc(pieces, width, height, cam, data, sampleCount, world, lights);});

    }

    for (int i = 0; i < threads.size(); ++i) {

            if (threads[i].joinable()) {
                threads[i].join();
            }

        }

    //threadFunc(pieces, width, height, cam, data, sampleCount, world);

    FILE * f = fopen("test.ppm", "w");

    outputImageData(data, width, height, f);

    fclose(f);

    std::cout << "Scaling data" << std::endl;

    double avgValue = 0.0;
    for (unsigned int i = 0; i < data.size(); ++i) {
        avgValue += data[i].length();
    }
    avgValue /= data.size();

    double scaleFactor = 0.5 / avgValue;
    for (unsigned int i = 0; i < data.size(); ++i) {
        data[i] = data[i] * scaleFactor;
    }

    f = fopen("test_scaled.ppm", "w");

    outputImageData(data, width, height, f);

    fclose(f);

    return 0;
}
