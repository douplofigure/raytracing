#ifndef SPHERE_H
#define SPHERE_H

#include "hitable.h"

class Sphere : public Hitable
{
    public:
        Sphere();
        Sphere(Vector<3> center, double radius, std::shared_ptr<Material>);
        virtual ~Sphere();

        bool hit(Ray & r, double t_min, double t_max, HitRecord & record);

    protected:

    private:

        Vector<3> center;
        double radius;
        std::shared_ptr<Material> material;

};

#endif // SPHERE_H
