#include "quaternion.h"

#include <cmath>

Quaternion::Quaternion(double a, double b, double c, double d) {

    this->a = a;
    this->b = b;
    this->c = c;
    this->d = d;

}

Quaternion::Quaternion(double a, Vector<3> vec) {

    this->a = a;
    this->b = vec[0];
    this->c = vec[1];
    this->d = vec[2];

}

Quaternion::Quaternion() {
    a = 0;
    b = 0;
    c = 0;
    d = 0;
}

Quaternion::~Quaternion()
{
    //dtor
}

Vector<3> Quaternion::getVectorPart() {
    return Vector<3>(b,c,d);
}

double Quaternion::getLinearPart() {
    return a;
}

Matrix<3,3> Quaternion::toRotationMatrix() {

    std::vector<double> values(9);

    values[0] = a * a + b * b - c * c - d * d;
    values[1] = 2 * b * c - 2 * a * d;
    values[2] = 2 * b * d + 2 * a * c;
    values[3] = 2 * b * c + 2 * a * d;
    values[4] = a * a - b * b + c * c - d * d;
    values[5] = 2 * c * d - 2 * a * b;
    values[6] = 2 * b * d - 2 * a * c;
    values[7] = 2 * c * d + 2 * a * b;
    values[8] = a * a - b * b - c * c + d * d;

    return Matrix<3,3>(values.data());

}

Quaternion Quaternion::operator+(Quaternion q) {

    return Quaternion(a + q.a, b + q.b, c + q.c, d + q.d);

}

Quaternion Quaternion::operator+(double f) {

    return Quaternion(a + f, b, c, d);

}

Quaternion Quaternion::operator-(Quaternion q) {

    return Quaternion(a - q.a, b - q.b, c - q.c, d - q.d);

}

Quaternion Quaternion::operator-(double f) {

    return Quaternion(a - f, b, c, d);

}

Quaternion Quaternion::operator=(double d) {
    return Quaternion(d,0,0,0);
}

Vector<3> cross(Vector<3> vec1, Vector<3> vec2) {
    return Vector<3>(vec1[1] * vec2[2] - vec1[2] * vec2[1], -vec1[2] * vec2[0] + vec1[0] * vec2[2], vec1[0] * vec2[1] - vec1[1] * vec2[0]);
}

Quaternion Quaternion::operator*(Quaternion q) {

    Vector<3> v0 = this->getVectorPart();
    Vector<3> v1 = q.getVectorPart();

    return Quaternion(a * q.a - v0 * v1, a * v1 + q.a * v0 + cross(v0, v1));

}

Quaternion Quaternion::operator*(double d)  {
    return *this * Quaternion(d, 0, 0, 0);
}

Quaternion Quaternion::operator/(Quaternion q) {

    return *this * q.conjugate() / q.norm();

}

Quaternion Quaternion::operator/(double d) {
    return Quaternion(a/d, b/d, c/d, this->d / d);
}

Quaternion Quaternion::conjugate() {

    return Quaternion(a, -b, -c, -d);

}

double Quaternion::norm() {

    return sqrt(a*a + b*b + c*c + d*d);

}

Quaternion Quaternion::fromAxisAngle(Vector<3> axis, double angle) {

    axis.normalize();

    return Quaternion(cos(angle / 2.0), axis * sin(angle / 2.0));

}
