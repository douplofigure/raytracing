#include "light.h"

#include <limits>

#include "ray.h"

Light::Light() {
    //ctor
}

Light::Light(std::shared_ptr<Spectrum> s) {
    this->spec = s;
}

Light::~Light()
{
    //dtor
}

std::shared_ptr<Spectrum> Light::getSpectrum() {
    return spec;
}

double Light::getArea() {
    return 1.0;
}


PointLight::PointLight(Vector<3> p, Vector<3> c) {
    this->color = c;
    this->pos = p;
}

PointLight::PointLight(Vector<3> p, std::shared_ptr<Spectrum> c) : Light(c) {
    this->pos = p;
}

Vector<3> PointLight::getIntensityAt(HitRecord & rec) {

    Vector<3> L = pos - rec.p;

    double r2 = L.length2();
    double cosTheta = 2 * (L * rec.normal) / (rec.normal.length() * sqrt(r2));

    return (cosTheta / r2) * color;

}

double PointLight::getFalloff(HitRecord & rec) {

    Vector<3> L = pos - rec.p;

    double r2 = L.length2();
    double cosTheta = 2 * (L * rec.normal) / (rec.normal.length() * sqrt(r2));

    return (cosTheta / r2);

}

double PointLight::getArea() {
    return 0.001;
}

bool PointLight::checkReached(std::shared_ptr<Hitable> world, HitRecord & rec) {

    HitRecord tmp;

    Vector<3> L = rec.p - pos;

    Ray r(pos, L);

    return !world->hit(r, 0.0, 1.0, tmp);

}

SunLight::SunLight(Vector<3> p, Vector<3> c) {
    this->color = c;
    this->dir = p;
    this->dir.normalize();
}

SunLight::SunLight(Vector<3> p, std::shared_ptr<Spectrum> spec) : Light(spec) {

    this->dir = p;
    this->dir.normalize();

}

Vector<3> SunLight::getIntensityAt(HitRecord & rec) {

    Vector<3> L = -dir;

    double r2 = L.length2();
    double cosTheta = 2 * (L * rec.normal) / (rec.normal.length() * sqrt(r2));

    return (cosTheta / r2) * color;

}

double SunLight::getFalloff(HitRecord & rec) {

    Vector<3> L = -dir;

    double r2 = L.length2();
    double cosTheta = 2 * (L * rec.normal) / (rec.normal.length() * sqrt(r2));

    //std::cout << "Falloff " << (cosTheta / (150e9)) << std::endl;

    return cosTheta * 0.003;

}

bool SunLight::checkReached(std::shared_ptr<Hitable> world, HitRecord & rec) {

    HitRecord tmp;

    Vector<3> L = -dir;

    Ray r(rec.p, L);

    return !world->hit(r, 0.0, std::numeric_limits<double>::max(), tmp);

}
