#ifndef HITABLE_H
#define HITABLE_H

#define SCALAR_TYPE float

#include <memory>

#include "vector.h"
#include "ray.h"

class Material;

struct HitRecord {
    double t;
    Vector<3> p;
    Vector<3> normal;
    std::shared_ptr<Material> mat;
};

class Hitable {
    public:
        virtual bool hit(Ray & r, double t_min, double t_max, HitRecord & record) = 0;
};

#endif // HITABLE_H
