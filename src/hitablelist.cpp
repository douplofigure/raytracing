#include "hitablelist.h"

HitableList::HitableList() {
    //ctor
}

HitableList::HitableList(std::vector<std::shared_ptr<Hitable>> & data) {
    this->objects = std::vector<std::shared_ptr<Hitable>>(data.size());
    for (int i = 0; i < data.size(); ++i) {
        objects[i] = data[i];
    }
}

HitableList::~HitableList()
{
    //dtor
}

bool HitableList::hit(Ray& r, double t_min, double t_max, HitRecord& rec) {

    HitRecord tempRec;
    bool hitAnything = false;
    double closest = t_max;

    for (int i = 0; i < objects.size(); ++i) {
        if (objects[i]->hit(r, t_min, closest, tempRec)) {
            hitAnything = true;
            closest = tempRec.t;
            rec = tempRec;
        }
    }

    return hitAnything;

}

void HitableList::addObject(std::shared_ptr<Hitable> obj) {
    objects.push_back(obj);
}
