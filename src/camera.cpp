#include "camera.h"

Camera::Camera() {

    llcorner = Vector<3>(-2.0, 1.0, -1.0);
    horizontal = Vector<3>(4.0, 0.0, 0.0);
    vertical = Vector<3>(0.0, 0.0, 2.0);
    position = Vector<3>(0.0, 0.0, 0.0);

    rotation = Quaternion::fromAxisAngle(Vector<3>(1.0, 0.0, 1.0), - M_PI / 4.0);

}

Camera::Camera(double vfov, double aspect, Vector<3> pos, Quaternion rot) {
    double theta = vfov * M_PI / 180.0;
    double halfH = tan(theta / 2.0);
    double halfW = aspect * halfH;

    llcorner = Vector<3>(-halfW, 1.0, -halfH);
    horizontal = Vector<3>(2.0*halfW, 0.0, 0.0);
    vertical = Vector<3>(0.0, 0.0, 2.0*halfH);
    position = pos;

    rotation = rot;

}

Camera::~Camera()
{
    //dtor
}

Ray Camera::getRay(double u, double v) {

    Vector<3> direction = rotation.toRotationMatrix() * (llcorner + u*horizontal + v*vertical);

    return Ray(position, direction);
}
