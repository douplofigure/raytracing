#include "sphere.h"

Sphere::Sphere() {

}

Sphere::Sphere(Vector<3> center, double radius, std::shared_ptr<Material> mat) {
    this->radius = radius;
    this->center = center;
    this->material = mat;
}

Sphere::~Sphere() {

}

bool Sphere::hit(Ray & r, double t_min, double t_max, HitRecord& rec) {

    Vector<3> oc = r.origin() - center;
    double a = r.direction() * r.direction();
    double b = 2.0 * (oc * r.direction());
    double c = (oc * oc) - radius * radius;
    double delta = b*b - 4 * a * c;

    if (delta < 0) return false;

    double temp = (-b - sqrt(delta)) / (2.0 * a);
    if (temp >= t_min && temp <= t_max) {
        rec.t = temp;
        rec.p = r.pointAt(rec.t);
        rec.normal = (rec.p - center) / radius;
        rec.mat = material;
        return true;
    }

    temp = (-b + sqrt(delta)) / (2.0 * a);
    if (temp >= t_min && temp <= t_max) {
        rec.t = temp;
        rec.p = r.pointAt(rec.t);
        rec.normal = (rec.p - center) / radius;
        rec.mat = material;
        return true;
    }

    return false;

}
