#ifndef LIGHT_H
#define LIGHT_H

#include "vector.h"
#include "hitable.h"

#include "color/spectrum.h"

class Light
{
    public:
        Light();
        Light(std::shared_ptr<Spectrum> spec);
        virtual ~Light();

        virtual Vector<3> getIntensityAt(HitRecord & rec) = 0;
        virtual double getFalloff(HitRecord & rec) = 0;
        virtual double getArea();
        virtual bool checkReached(std::shared_ptr<Hitable> world, HitRecord& rec) = 0;
        virtual std::shared_ptr<Spectrum> getSpectrum();

    protected:

        std::shared_ptr<Spectrum> spec;

    private:
};

class PointLight : public Light {

    public:

        PointLight(Vector<3> pos, Vector<3> color);
        PointLight(Vector<3> pos, std::shared_ptr<Spectrum> spec);

        Vector<3> getIntensityAt(HitRecord & rec) override;
        double getFalloff(HitRecord & rec) override;
        double getArea() override;
        bool checkReached(std::shared_ptr<Hitable> world, HitRecord& rec)override;

    private:
        Vector<3> pos;
        Vector<3> color;

};

class SunLight : public Light {

    public:
        SunLight(Vector<3> direction, Vector<3> color);
        SunLight(Vector<3> direction, std::shared_ptr<Spectrum> spec);

        Vector<3> getIntensityAt(HitRecord & rec) override;
        double getFalloff(HitRecord & rec) override;
        bool checkReached(std::shared_ptr<Hitable> world, HitRecord& rec)override;

    private:
        Vector<3> dir;
        Vector<3> color;


};

#endif // LIGHT_H
