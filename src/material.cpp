#include "material.h"

Vector<3> randomInUnitSpere() {
    Vector<3> p;

    do {

        double x = drand48();
        double y = drand48();
        double z = drand48();

        p = 2.0 * (Vector<3>(x, y, z) - Vector<3>(1.0, 1.0, 1.0));

    } while (p.length() >= 1.0);

    return p;

}

bool Material::scatter(Ray & r, HitRecord & rec, std::shared_ptr<Spectrum> & refSpec, Ray & scattered, std::shared_ptr<Hitable> world) {

    Vector<3> target = rec.normal + randomInUnitSpere();
    scattered = Ray(rec.p, target);

    refSpec = std::shared_ptr<Spectrum>(new ConstantSpectrum(1.0));

    return true;

}

Vector<3> reflect(Vector<3> v, Vector<3> n) {
    return v - 2.0 * (v * n) * n;
}

Lambertian::Lambertian(Vector<3> al) {
    this->albedo = al;
    this->refSpec = std::shared_ptr<Spectrum>(new CIESpectrum(albedo));
}

bool Lambertian::scatter(Ray & r, HitRecord & rec, Vector<3>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world) {

    Vector<3> target = rec.normal + randomInUnitSpere();
    scattered = Ray(rec.p, target);
    attenuation = albedo;

    return true;

}

bool Lambertian::scatter(Ray & r, HitRecord & rec, std::shared_ptr<Spectrum>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world) {

    Vector<3> target = rec.normal + randomInUnitSpere();
    scattered = Ray(rec.p, target);
    attenuation = refSpec;

    return true;

}

Metal::Metal(Vector<3> al) {
    this->albedo = al;
}

Metal::Metal(std::shared_ptr<Spectrum> spec) {
    this->refSpec = spec;
}

bool Metal::scatter(Ray & r, HitRecord & rec, Vector<3>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world) {

    Vector<3> reflected = reflect(r.direction().normalized(), rec.normal);
    scattered = Ray(rec.p, reflected);
    attenuation = albedo;

    return (scattered.direction() * rec.normal) > 0;

}

bool Metal::scatter(Ray & r, HitRecord & rec, std::shared_ptr<Spectrum>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world) {

    Vector<3> reflected = reflect(r.direction().normalized(), rec.normal);
    scattered = Ray(rec.p, reflected);
    attenuation = this->refSpec;

    return (scattered.direction() * rec.normal) > 0;

}

Glass::Glass(double ior) {
    this->ior = ior;
}

Vector<3> refract(Vector<3> &I, Vector<3> &N, double &ior) {
    double cosi = I * N;
    double etai = 1, etat = ior;
    Vector<3> n = N;
    if (cosi < 0) { cosi = -cosi; } else { std::swap(etai, etat); n= -1.0 * N; }
    double eta = etai / etat;
    double k = 1 - eta * eta * (1 - cosi * cosi);
    return k < 0 ? Vector<3>(0.0, 0.0, 0.0) : eta * I + (eta * cosi - sqrtf(k)) * n;
}

bool Glass::scatter(Ray & r, HitRecord & rec, Vector<3>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world) {

    double cosi = r.direction() * rec.normal;
    double etai = 1, etat = ior;
    Vector<3> n = rec.normal;

    if (cosi < 0) {
        cosi = -cosi;
    } else {
        std::swap(etai, etat);
        n= -1.0 * rec.normal;
    }

    double eta = etai / etat;
    double k = 1 - eta * eta * (1 - cosi * cosi);

    Vector<3> refracted;

    if (k < 0) {

        refracted = reflect(r.direction().normalized(), rec.normal);
        return false;

    } else {

        refracted = eta * r.direction() + (eta * cosi - sqrt(k)) * n;
    }

    if (!(refracted[0] == refracted[0] && refracted[1] == refracted[1] && refracted[2] == refracted[2]))
        std::cerr << "Some nan value exists in " << refracted << std::endl;

    scattered = Ray(rec.p, refracted.normalized());
    attenuation = Vector<3>(1.0, 1.0, 1.0);

    return true;

}

bool Glass::scatter(Ray & r, HitRecord & rec, std::shared_ptr<Spectrum>& attenuation, Ray& scattered, std::shared_ptr<Hitable> world) {

    double cosi = r.direction() * rec.normal;
    double etai = 1, etat = ior;
    Vector<3> n = rec.normal;

    if (cosi < 0) {
        cosi = -cosi;
    } else {
        std::swap(etai, etat);
        n= -1.0 * rec.normal;
    }

    double eta = etai / etat;
    double k = 1 - eta * eta * (1 - cosi * cosi);

    Vector<3> refracted;

    if (k < 0) {

        refracted = reflect(r.direction().normalized(), rec.normal);
        return false;

    } else {

        refracted = eta * r.direction() + (eta * cosi - sqrt(k)) * n;
    }

    if (!(refracted[0] == refracted[0] && refracted[1] == refracted[1] && refracted[2] == refracted[2]))
        std::cerr << "Some nan value exists in " << refracted << std::endl;

    scattered = Ray(rec.p, refracted.normalized());
    attenuation = std::shared_ptr<Spectrum>(new ConstantSpectrum(0.9));

    return true;

}
